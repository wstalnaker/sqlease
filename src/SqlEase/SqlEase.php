<?php namespace SqlEase;

/**
 * Created by PhpStorm.
 * User: bstalnaker
 * Date: 5/3/2017
 * Time: 4:45 PM
 */
class SqlEase
{
	protected $input;

	public function __construct()
	{
	}

	public function run()
	{
		//the main jane
		//decode url, call classes and methods
		$path = $path = explode('/', __FILE__);// Splitting the file-name, removing the extension
		$name = explode('.', $path[count($path) - 1]);

		// Uppercasing the first letter to be nice and OOP-ish
		$classToCall = ucfirst($name[0]);

		// Creating a new instance
		$controller = new $classToCall();
		if($controller instanceof self)
		{
			// Logging
			$controller->doLog();

			// Printing the final response
			$controller->printResponse();
		}
	}

	protected function printResponse()
	{
	}

	protected function doLog()
	{
	}
}